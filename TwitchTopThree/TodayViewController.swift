import UIKit
import NotificationCenter
import AlamofireImage

class TodayViewController: UIViewController, NCWidgetProviding {
    @IBOutlet weak var stackView: UIStackView!
    var spinner: UIActivityIndicatorView!
    var presenter: TodayPresenter!
    @IBOutlet var namesLabel: [UILabel]!
    @IBOutlet var imagesView: [UIImageView]!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.spinner = UIActivityIndicatorView(activityIndicatorStyle: .gray)
        self.spinner.center = self.stackView.center
        self.spinner.startAnimating()
        self.view.addSubview(self.spinner)
        self.stackView.alpha = 0
        
        self.namesLabel.sort(by: { $0.tag < $1.tag })
        self.imagesView.sort(by: { $0.tag < $1.tag })
    
        self.presenter = TodayPresenter()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        
        self.presenter.getTopTreeGames(success: { (topGames) in
            var i = 0
            for game in topGames {
                self.namesLabel[i].text = game.name
                if let imageURL = URL(string: game.boxImageURL) {
                    self.imagesView[i].af_setImage(withURL: imageURL)
                }
                
                i += 1
            }
            
            UIView.animate(withDuration: 0.4, animations: {
                self.stackView.alpha = 1
            }, completion: { _ in
                self.spinner.stopAnimating()
            })
        }) {
            
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func widgetPerformUpdate(completionHandler: (@escaping (NCUpdateResult) -> Void)) {
        // Perform any setup necessary in order to update the view.
        
        // If an error is encountered, use NCUpdateResult.Failed
        // If there's no update required, use NCUpdateResult.NoData
        // If there's an update, use NCUpdateResult.NewData
        
        completionHandler(NCUpdateResult.newData)
    }
    
}
