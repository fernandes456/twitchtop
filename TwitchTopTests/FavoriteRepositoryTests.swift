import XCTest
@testable import TwitchTop

class FavoriteRepositoryTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        
        var repository = FavoriteRepository()
        repository.deleteAllFavoritesGames()
    }
    
    override func tearDown() {
        super.tearDown()
        
        var repository = FavoriteRepository()
        repository.deleteAllFavoritesGames()
    }
    
    func testFavoriteGame() {
        var repository = FavoriteRepository()
        
        let game = Game(id: 123, name: "[gfsf]", boxImageURL: "http://www.google.com.br", viewers: nil)
        
        repository.changeFavoriteStatus(game: game, success: { (wasFavorited) in
            XCTAssert(wasFavorited)
        }) {
            XCTFail()
        }
        let games = repository.getAllFavoritesGames()
        XCTAssertEqual(game, games[0])
    }
    
    func testListGame() {
        var repository = FavoriteRepository()
        let games = [
            Game(id: 121, name: "[gfsf] 1", boxImageURL: "http://www.google.com.br", viewers: nil),
            Game(id: 124, name: "[gfsf] 4", boxImageURL: "http://www.google.com.br", viewers: nil),
            Game(id: 125, name: "[gfsf] 5", boxImageURL: "http://www.google.com.br", viewers: nil),
            Game(id: 126, name: "[gfsf] 6", boxImageURL: "http://www.google.com.br", viewers: nil),
            Game(id: 127, name: "[gfsf] 7", boxImageURL: "http://www.google.com.br", viewers: nil),
            Game(id: 128, name: "[gfsf] 8", boxImageURL: "http://www.google.com.br", viewers: nil)
        ]

        let emptyGames = repository.getAllFavoritesGames()
        XCTAssertEqual(emptyGames.count, 0)
        
        for game in games {
            repository.add(game: game, success: nil, failure: nil)
        }

        let loadGames = repository.getAllFavoritesGames()
        XCTAssertEqual(loadGames.count, 6)
        for i in 0..<loadGames.count {
            XCTAssertEqual(loadGames[i], games[i])
        }
    }
}
