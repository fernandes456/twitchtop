import Foundation

struct GameJSON: Decodable {
    let viewers: Int
    let game: GameDetailsJSON
    
    func getGame() -> Game {
        return Game(id: self.game._id, name: self.game.name, boxImageURL: self.game.box.large, viewers: viewers)
    }
}

struct GameDetailsJSON: Decodable {
    let _id: Int
    let name: String
    let box: BoxJSON
}

struct BoxJSON: Decodable {
    let large: String
}
