import Foundation

struct Game: Equatable {
    let id: Int
    let name: String
    let boxImageURL: String
    let viewers: Int?
    
    public static func ==(lhs: Game, rhs: Game) -> Bool {
        return lhs.id == rhs.id
    }
}
