import UIKit
import Alamofire
import AlamofireImage

protocol GameCellProtocol {
    func changeFavoriteStatus(game: Game, cell: GameCell)
}

class GameCell: UICollectionViewCell {
    @IBOutlet private weak var gameImageView: UIImageView!
    @IBOutlet private weak var nameLabel: UILabel!
    @IBOutlet private  weak var favoriteButton: AnimateButton!
    var delegate: GameCellProtocol?
    private var game: Game?
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let origImage = UIImage(named: "ic_favorite")
        let tintedImage = origImage?.withRenderingMode(.alwaysTemplate)
        self.favoriteButton.setImage(tintedImage, for: .normal)
        self.favoriteButton.tintColor = .lightGray
    }
    
    @IBAction func favoriteTouched() {
        self.favoriteButton.select()
        guard let game = self.game else { return }
        self.delegate?.changeFavoriteStatus(game: game, cell: self)        
    }
    
    func fillCell(game: Game, isFavorite: Bool) {
        self.game = game
        self.nameLabel.text = game.name
        if let url = URL(string: game.boxImageURL) {
            self.gameImageView.af_setImage(withURL: url)
        }
        
        self.setFavorite(isFavorite)
    }
    
    func setFavorite(_ isFavorite: Bool) {
        self.favoriteButton.tintColor = isFavorite ? .red : .lightGray
    }
    
    func setDisableFavoriteButton() {
        self.favoriteButton.isUserInteractionEnabled = false
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        
        self.gameImageView.af_cancelImageRequest()
        self.nameLabel.text = ""
        self.game = nil
        self.favoriteButton.deselect()
    }
}
