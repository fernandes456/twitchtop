import Foundation
import Alamofire

protocol GameRepositoryType {
    func getGames(limit: Int,
                  offset: Int,
                  success: @escaping ([Game]) -> (),
                  failure: @escaping () -> ())
}

struct GameRepository: GameRepositoryType {
    private let kBaseURL = "https://api.twitch.tv/kraken/games/top"

     func getGames(limit: Int,
                           offset: Int,
                           success: @escaping ([Game]) -> (),
                           failure: @escaping () -> ()) {
        
        let parameters: Parameters = ["limit": limit,
                                      "offset": offset]
        
        let headers = [
            "Client-ID": "vwgwwy6k5rv3godjh4q8gxk3ekm21q"
        ]
        
        Alamofire.request(kBaseURL, parameters: parameters, headers: headers).responseJSON { response in
            if let json = response.result.value as? [String: Any],
                let gamesJson = json["top"] {
                var games = [Game]()
                if let gameJson = gamesJson as? Array<NSDictionary> {
                    
                    for gameDict in gameJson {
                        if let jsonData = try? JSONSerialization.data(withJSONObject: gameDict),
                            let game = try? JSONDecoder().decode(GameJSON.self, from: jsonData)
                        {
                            games.append(game.getGame())
                        }
                    }
                }
                
                success(games)
            } else {
                failure()
            }
        }
    }
}
