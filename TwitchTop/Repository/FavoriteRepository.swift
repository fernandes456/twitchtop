import Foundation
import CoreData

struct FavoriteRepository {
    
    // MARK: - Core Data stack
    
    lazy var persistentContainer: NSPersistentContainer = {
        /*
         The persistent container for the application. This implementation
         creates and returns a container, having loaded the store for the
         application to it. This property is optional since there are legitimate
         error conditions that could cause the creation of the store to fail.
         */
        let container = NSPersistentContainer(name: "TwitchTop")
        container.loadPersistentStores(completionHandler: { (storeDescription, error) in
            if let error = error as NSError? {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                
                /*
                 Typical reasons for an error here include:
                 * The parent directory does not exist, cannot be created, or disallows writing.
                 * The persistent store is not accessible, due to permissions or data protection when the device is locked.
                 * The device is out of space.
                 * The store could not be migrated to the current model version.
                 Check the error message to determine what the actual problem was.
                 */
                fatalError("Unresolved error \(error), \(error.userInfo)")
            }
        })
        return container
    }()
    
    // MARK: - Core Data Saving support
    mutating func saveContext (success: (() -> ())?, failure: (() -> ())?) {
        let context = persistentContainer.viewContext
        if context.hasChanges {
            do {
                try context.save()
                success?()
            } catch {
                // Replace this implementation with code to handle the error appropriately.
                // fatalError() causes the application to generate a crash log and terminate. You should not use this function in a shipping application, although it may be useful during development.
                let nserror = error as NSError
                failure?()
                fatalError("Unresolved error \(nserror), \(nserror.userInfo)")
            }
        }
    }
    
    // MARK: -
    
    mutating func changeFavoriteStatus(game: Game, success: @escaping (_ wasFavorited: Bool) -> (), failure: @escaping () -> ()) {
        if self.isFavorite(game: game) {
            self.deleteGame(game: game, success: {
                success(false)
            }, failure: failure)
        } else {
            self.add(game: game, success: {
                success(true)
            }, failure: failure)
        }
    }
    
    mutating func add(game: Game, success: (() -> ())?, failure: (() -> ())?) {
        let context = persistentContainer.viewContext
        let entity = NSEntityDescription.entity(forEntityName: "GameEntity", in: context)
        let newGame = NSManagedObject(entity: entity!, insertInto: context)
        
        newGame.setValue(game.id, forKey: "id")
        newGame.setValue(game.name, forKey: "name")
        newGame.setValue(game.boxImageURL, forKey: "boxImageURL")
        
        self.saveContext(success: success, failure: failure)
    }
    
    mutating func isFavorite(game: Game) -> Bool {
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GameEntity")
        request.predicate = NSPredicate(format: "id = \(game.id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for _ in result as! [NSManagedObject] {
                return true
            }
        } catch {
            print("Failed")
        }
        
        return false
    }
    
    mutating func getAllFavoritesGames() -> [Game] {
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GameEntity")
        request.returnsObjectsAsFaults = false
        var favoritesGames = [Game]()
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                guard let id = data.value(forKey: "id") as? Int,
                let name = data.value(forKey: "name") as? String,
                let boxImageURL = data.value(forKey: "boxImageURL") as? String else {
                    print("Deu ruim")
                    continue
                }
                
                favoritesGames.append(Game(id: id, name: name, boxImageURL: boxImageURL, viewers: nil))
            }
            
        } catch {
            
            print("Failed")
        }
        
        return favoritesGames
    }
    
    mutating func deleteGame(game: Game, success: (() -> ())?, failure: (() -> ())?) {
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GameEntity")
        request.predicate = NSPredicate(format: "id = \(game.id)")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                context.delete(data)
            }
        } catch {
            print("Failed")
        }
        
        self.saveContext(success: success, failure: failure)
    }
    
    mutating func deleteAllFavoritesGames() {
        let context = persistentContainer.viewContext
        let request = NSFetchRequest<NSFetchRequestResult>(entityName: "GameEntity")
        request.returnsObjectsAsFaults = false
        do {
            let result = try context.fetch(request)
            for data in result as! [NSManagedObject] {
                context.delete(data)
            }
            
        } catch {
            print("Failed")
        }
        
        self.saveContext(success: nil, failure: nil)
    }

}
