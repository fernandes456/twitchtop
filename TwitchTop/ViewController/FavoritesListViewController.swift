import UIKit
import Toast_Swift

class FavoritesListViewController: UIViewController {

    @IBOutlet weak var collectionView: UICollectionView!
    var refresher: UIRefreshControl!
    var presenter: FavoritesListPresenter!
    private let kDetailsScreenSegue = "DetailScreenSegue"
    @IBOutlet weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter = FavoritesListPresenter(delegate: self)
        self.collectionView.register(UINib(nibName: "GameCell", bundle: nil), forCellWithReuseIdentifier: "GameCell")
        self.collectionView.dataSource = self.presenter
        self.collectionView.delegate = self.presenter
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = .black
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView.addSubview(self.refresher)
        
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.presenter.reloadData()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let viewController = segue.destination as? FavoriteViewController,
            let game = sender as? Game {
            
            viewController.game = game
        }
    }
    
    @objc func loadData() {
        self.presenter.reloadData()
    }
}

extension FavoritesListViewController: FavoritesListPresenterDelegate {
    func reloadData() {
        self.errorView.isHidden = true
        self.view.sendSubview(toBack: self.errorView)
        self.collectionView.reloadData()
        self.refresher.endRefreshing()
    }
    
    func showDetailsFor(game: Game) {
        self.performSegue(withIdentifier: kDetailsScreenSegue, sender: game)
    }
    
    func show(message: String) {
        self.view.makeToast(message, duration: 3.0)
    }
    
    func showEmptyList() {
        self.errorView.isHidden = false
        self.view.bringSubview(toFront: self.errorView)
    }
}
