import UIKit
import Alamofire
import AlamofireImage
import Toast_Swift

class FavoriteViewController: UIViewController {

    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var totalViewsLabel: UILabel!
    var game: Game!
    var presenter: FavoritePresenter!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
        
        guard let game = self.game else {
            self.show(message: String.localize(key: "message.error.default", comment: ""))
            self.navigationController?.popViewController(animated: true)
            return
        }
        self.presenter = FavoritePresenter(delegate: self)
        self.nameLabel.text = game.name
        self.title = game.name
        if let viewers = game.viewers {
            self.totalViewsLabel.text = "Número de visualizações: \(viewers)"
        } else {
            self.totalViewsLabel.isHidden = true
        }
        
        if let url = URL(string: game.boxImageURL)
        {
            self.imageView.af_setImage(withURL: url)
        }
        
        self.gameIs(favorite: self.presenter.isFavorite(game: game))
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    @IBAction func favoriteTouched(_ sender: Any) {
        self.presenter.changeFavoriteStatus(game: self.game)
    }
    
    func gameIs(favorite: Bool) {
        self.navigationItem.rightBarButtonItem?.tintColor = favorite ? .red : .lightGray
    }
}

extension FavoriteViewController: FavoritePresenterDelegate {
    func changeFavoriteStatus(wasFavorited: Bool) {
//        self.navigationItem.rightBarButtonItem?.tintColor = wasFavorited ? .red : .lightGray
        self.gameIs(favorite: wasFavorited)
    }
    
    func show(message: String) {
        self.view.makeToast(message, duration: 3.0)
    }
}
