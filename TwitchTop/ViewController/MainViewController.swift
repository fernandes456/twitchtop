import UIKit
import Toast_Swift

class MainViewController: UIViewController {

    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var collectionView: UICollectionView!
    var presenter: MainPresenter!
    var refresher: UIRefreshControl!
    private let kDetailsScreenSegue = "DetailScreenSegue"
    @IBOutlet weak var errorView: UIView!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        self.presenter = MainPresenter(delegate: self)
        self.collectionView.register(UINib(nibName: "GameCell", bundle: nil), forCellWithReuseIdentifier: "GameCell")
        self.collectionView.dataSource = self.presenter
        self.collectionView.delegate = self.presenter
        self.collectionView.dragInteractionEnabled = true
        self.collectionView.dragDelegate = self.presenter
        self.collectionView.dropDelegate = self.presenter
        self.refresher = UIRefreshControl()
        self.refresher.tintColor = .black
        self.refresher.addTarget(self, action: #selector(loadData), for: .valueChanged)
        self.collectionView.addSubview(self.refresher)
        self.searchBar.delegate = self.presenter
        
        ToastManager.shared.isTapToDismissEnabled = true
        ToastManager.shared.isQueueEnabled = true
        
        self.presenter.viewDidLoad()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        super.prepare(for: segue, sender: sender)
        
        if let viewController = segue.destination as? FavoriteViewController,
            let game = sender as? Game {
            
            viewController.game = game
        }
    }

    @objc func loadData() {
        self.presenter.getFirstPage()
    }
    
    func stopRefresher() {
        self.refresher.endRefreshing()
    }
    
    @IBAction func retryTouched() {
        self.presenter.getFirstPage()
    }
}

extension MainViewController: MainPresenterDelegate {
    func reloadData() {
        if !self.errorView.isHidden {
            UIView.animate(withDuration: 0.4, animations: {
                self.errorView.alpha = 0
            }, completion: { (completed) in
                self.errorView.isHidden = true
                self.errorView.alpha = 1
                self.view.sendSubview(toBack: self.errorView)
            })
        }
        self.collectionView.reloadData()
    }
    
    func showDetailsFor(game: Game) {
        self.performSegue(withIdentifier: kDetailsScreenSegue, sender: game)
    }
    
    func endLoadData() {
        self.stopRefresher()
    }
    
    func reloadCellFor(index: IndexPath) {
        self.collectionView.reloadItems(at: [index])
    }
    
    func showNoInternetError() {
        self.errorView.isHidden = false
        self.view.bringSubview(toFront: self.errorView)
    }
    
    func show(message: String) {
        self.view.makeToast(message, duration: 3.0)
    }
}
