import UIKit

@IBDesignable
open class AnimateButton: UIButton {

    fileprivate let kLinesCount = 5

    fileprivate var imageShape: CAShapeLayer!

    @IBInspectable open var image: UIImage! {
        didSet {
            createLayers(image: image)
        }
    }

    @IBInspectable open var imageColorOn: UIColor! = UIColor.defaultColor() {
        didSet {
            if (isSelected) {
                imageShape.fillColor = imageColorOn.cgColor
            }
        }
    }

    @IBInspectable open var imageColorOff: UIColor! = UIColor.colorOff() {
        didSet {
            if (!isSelected) {
                imageShape.fillColor = imageColorOff.cgColor
            }
        }
    }

    fileprivate var circleShape: CAShapeLayer!
    fileprivate var circleMask: CAShapeLayer!
    @IBInspectable open var circleColor: UIColor! = UIColor.defaultColor() {
        didSet {
            circleShape.fillColor = circleColor.cgColor
        }
    }

    fileprivate var lines: [CAShapeLayer]!
    @IBInspectable open var lineColor: UIColor! = UIColor.line() {
        didSet {
            for line in lines {
                line.strokeColor = lineColor.cgColor
            }
        }
    }

    fileprivate let circleTransform = CAKeyframeAnimation(keyPath: "transform")
    fileprivate let circleMaskTransform = CAKeyframeAnimation(keyPath: "transform")
    fileprivate let lineStrokeStart = CAKeyframeAnimation(keyPath: "strokeStart")
    fileprivate let lineStrokeEnd = CAKeyframeAnimation(keyPath: "strokeEnd")
    fileprivate let lineOpacity = CAKeyframeAnimation(keyPath: "opacity")
    fileprivate let imageTransform = CAKeyframeAnimation(keyPath: "transform")

    @IBInspectable open var duration: Double = 1.0 {
        didSet {
            circleTransform.duration = 0.333 * duration
            circleMaskTransform.duration = 0.333 * duration
            lineStrokeStart.duration = 0.6 * duration
            lineStrokeEnd.duration = 0.6 * duration
            lineOpacity.duration = 1.0 * duration
            imageTransform.duration = 1.0 * duration
        }
    }

    override open var isSelected : Bool {
        didSet {
            if (isSelected != oldValue) {
                if isSelected {
                    imageShape.fillColor = imageColorOn.cgColor
                } else {
                    deselect()
                }
            }
        }
    }

    public convenience init() {
        self.init(frame: CGRect.zero)
    }

    public override convenience init(frame: CGRect) {
        self.init(frame: frame, image: UIImage())
    }

    public init(frame: CGRect, image: UIImage!) {
        super.init(frame: frame)
        self.image = image
        createLayers(image: image)
        addTargets()
    }

    public required init(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)!
        createLayers(image: UIImage())
        addTargets()
    }

    fileprivate func createLayers(image: UIImage!) {
        self.layer.sublayers = nil

        let imageFrame = CGRect(x: frame.size.width / 2 - frame.size.width / 4, y: frame.size.height / 2 - frame.size.height / 4, width: frame.size.width / 2, height: frame.size.height / 2)
        let imgCenterPoint = CGPoint(x: imageFrame.midX, y: imageFrame.midY)
        let lineFrame = CGRect(x: imageFrame.origin.x - imageFrame.width / 4, y: imageFrame.origin.y - imageFrame.height / 4 , width: imageFrame.width * 1.5, height: imageFrame.height * 1.5)

        self.circleShape = CAShapeLayer()
        self.circleShape.bounds = imageFrame
        self.circleShape.position = imgCenterPoint
        self.circleShape.path = UIBezierPath(ovalIn: imageFrame).cgPath
        self.circleShape.fillColor = self.circleColor.cgColor
        self.circleShape.transform = CATransform3DMakeScale(0.0, 0.0, 1.0)
        self.layer.addSublayer(self.circleShape)

        self.circleMask = CAShapeLayer()
        self.circleMask.bounds = imageFrame
        self.circleMask.position = imgCenterPoint
        self.circleMask.fillRule = kCAFillRuleEvenOdd
        self.circleShape.mask = self.circleMask

        let maskPath = UIBezierPath(rect: imageFrame)
        maskPath.addArc(withCenter: imgCenterPoint, radius: 0.1, startAngle: CGFloat(0.0), endAngle: CGFloat(M_PI * 2), clockwise: true)
        self.circleMask.path = maskPath.cgPath

        self.lines = []
        for i in 0 ..< kLinesCount {
            let line = CAShapeLayer()
            line.bounds = lineFrame
            line.position = imgCenterPoint
            line.masksToBounds = true
            line.actions = ["strokeStart": NSNull(), "strokeEnd": NSNull()]
            line.strokeColor = lineColor.cgColor
            line.lineWidth = 1.25
            line.miterLimit = 1.25
            line.path = {
                let path = CGMutablePath()
                path.move(to: CGPoint(x: lineFrame.midX, y: lineFrame.midY))
                path.move(to: CGPoint(x: lineFrame.origin.x + lineFrame.width / 2, y: lineFrame.origin.y))
                return path
            }()
            line.lineCap = kCALineCapRound
            line.lineJoin = kCALineJoinRound
            line.strokeStart = 0.0
            line.strokeEnd = 0.0
            line.opacity = 0.0
            line.transform = CATransform3DMakeRotation(CGFloat(M_PI) / 5 * (CGFloat(i) * 2 + 1), 0.0, 0.0, 1.0)
            self.layer.addSublayer(line)
            self.lines.append(line)
        }

        self.imageShape = CAShapeLayer()
        self.imageShape.bounds = imageFrame
        self.imageShape.position = imgCenterPoint
        self.imageShape.path = UIBezierPath(rect: imageFrame).cgPath
        self.imageShape.fillColor = self.imageColorOff.cgColor
        self.imageShape.actions = ["fillColor": NSNull()]
        self.layer.addSublayer(self.imageShape)

        self.imageShape.mask = CALayer()
        self.imageShape.mask!.contents = image.cgImage
        self.imageShape.mask!.bounds = imageFrame
        self.imageShape.mask!.position = imgCenterPoint

        self.circleTransform.duration = 0.333
        self.circleTransform.values = [
            NSValue(caTransform3D: CATransform3DMakeScale(0.0,  0.0,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.5,  0.5,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.0,  1.0,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.2,  1.2,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.3,  1.3,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.37, 1.37, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.4,  1.4,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.4,  1.4,  1.0))
        ]
        self.circleTransform.keyTimes = [
            0.0,
            0.1,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            1.0
        ]

        self.circleMaskTransform.duration = 0.333
        self.circleMaskTransform.values = [
            NSValue(caTransform3D: CATransform3DIdentity),
            NSValue(caTransform3D: CATransform3DIdentity),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 1.25,  imageFrame.height * 1.25,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 2.688, imageFrame.height * 2.688, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 3.923, imageFrame.height * 3.923, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 4.375, imageFrame.height * 4.375, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 4.731, imageFrame.height * 4.731, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 5.0,   imageFrame.height * 5.0,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(imageFrame.width * 5.0,   imageFrame.height * 5.0,   1.0))
        ]
        self.circleMaskTransform.keyTimes = [
            0.0,
            0.2,
            0.3,
            0.4,
            0.5,
            0.6,
            0.7,
            0.9,
            1.0
        ]

        self.lineStrokeStart.duration = 0.6
        self.lineStrokeStart.values = [
            0.0,
            0.0,
            0.18,
            0.2,
            0.26,
            0.32,
            0.4,
            0.6,
            0.71,
            0.89,
            0.92
        ]
        lineStrokeStart.keyTimes = [
            0.0,
            0.056,
            0.111,
            0.167,
            0.222,
            0.278,
            0.333,
            0.389,
            0.444,
            0.944,
            1.0,
        ]

        self.lineStrokeEnd.duration = 0.6
        self.lineStrokeEnd.values = [
            0.0,
            0.0,
            0.32,
            0.48,
            0.64,
            0.68,
            0.92,
            0.92
        ]
        self.lineStrokeEnd.keyTimes = [
            0.0,
            0.056,
            0.111,
            0.167,
            0.222,
            0.278,
            0.944,
            1.0,
        ]

        self.lineOpacity.duration = 1.0
        self.lineOpacity.values = [
            1.0,
            1.0,
            0.0
        ]
        self.lineOpacity.keyTimes = [
            0.0,
            0.4,
            0.567
        ]

        self.imageTransform.duration = 1.0
        self.imageTransform.values = [
            NSValue(caTransform3D: CATransform3DMakeScale(0.0,   0.0,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.0,   0.0,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.2,   1.2,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.25,  1.25,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.2,   1.2,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.9,   0.9,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.875, 0.875, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.875, 0.875, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.9,   0.9,   1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.013, 1.013, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.025, 1.025, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(1.013, 1.013, 1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.96,  0.96,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.95,  0.95,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.96,  0.96,  1.0)),
            NSValue(caTransform3D: CATransform3DMakeScale(0.99,  0.99,  1.0)),
            NSValue(caTransform3D: CATransform3DIdentity)
        ]
        self.imageTransform.keyTimes = [
            0.0,
            0.1,
            0.3,
            0.333,
            0.367,
            0.467,
            0.5,
            0.533,
            0.567,
            0.667,
            0.7,
            0.733,
            0.833,
            0.867,
            0.9,
            0.967,
            1.0
        ]
    }

    fileprivate func addTargets() {
        self.addTarget(self, action: #selector(touchDown), for: UIControlEvents.touchDown)
        self.addTarget(self, action: #selector(touchUpInside), for: UIControlEvents.touchUpInside)
        self.addTarget(self, action: #selector(touchDragExit), for: UIControlEvents.touchDragExit)
        self.addTarget(self, action: #selector(touchDragEnter), for: UIControlEvents.touchDragEnter)
        self.addTarget(self, action: #selector(touchCancel), for: UIControlEvents.touchCancel)
    }

    @objc func touchDown(_ sender: AnimateButton) {
        self.layer.opacity = 0.4
    }
    @objc func touchUpInside(_ sender: AnimateButton) {
        self.layer.opacity = 1.0
    }
    @objc func touchDragExit(_ sender: AnimateButton) {
        self.layer.opacity = 1.0
    }
    @objc func touchDragEnter(_ sender: AnimateButton) {
        self.layer.opacity = 0.4
    }
    @objc func touchCancel(_ sender: AnimateButton) {
        self.layer.opacity = 1.0
    }

    open func select() {
        self.isSelected = true
        self.imageShape.fillColor = self.imageColorOn.cgColor

        CATransaction.begin()

        self.circleShape.add(self.circleTransform, forKey: "transform")
        self.circleMask.add(self.circleMaskTransform, forKey: "transform")
        self.imageShape.add(self.imageTransform, forKey: "transform")

        for i in 0 ..< kLinesCount {
            self.lines[i].add(self.lineStrokeStart, forKey: "strokeStart")
            self.lines[i].add(self.lineStrokeEnd, forKey: "strokeEnd")
            self.lines[i].add(self.lineOpacity, forKey: "opacity")
        }

        CATransaction.commit()
    }

    open func deselect() {
        self.isSelected = false
        self.imageShape.fillColor = self.imageColorOff.cgColor
        
        self.circleShape.removeAllAnimations()
        self.circleMask.removeAllAnimations()
        self.imageShape.removeAllAnimations()
        self.lines.forEach() {$0.removeAllAnimations()}
    }
}
