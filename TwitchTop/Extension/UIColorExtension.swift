import UIKit

extension UIColor {
    
    convenience init(red: Int, green: Int, blue: Int) {
        self.init(red: CGFloat(red)/255.0, green: CGFloat(green)/255.0, blue: CGFloat(blue)/255.0, alpha: 1.0)
    }
    
    static func defaultColor() -> UIColor {
        return UIColor(red: 253, green: 185, blue: 51)
    }
    
    static func colorOff() -> UIColor {
        return UIColor(red: 133, green: 156, blue: 166)
    }
    
    static func line() -> UIColor {
        return UIColor(red: 250, green: 120, blue: 68)
    }
}
