import Foundation
import UIKit
import Alamofire

protocol MainPresenterDelegate {
    func reloadData()
    func showDetailsFor(game: Game)
    func endLoadData()
    func show(message: String)
    func showNoInternetError()
}

class MainPresenter: NSObject {
    private var allGames: [Game] {
        didSet {
            if self.allGames != oldValue {
                self.filterGames()
            }
        }
    }
    private var filteredGames: [Game]
    private var currentPage: Int
    private let gameRepository: GameRepositoryType
    private var favoriteRepository: FavoriteRepository
    private let pageSize = 20
    private var nameFilter: String
    private var delegate: MainPresenterDelegate?
    
    init(delegate: MainPresenterDelegate?) {
        self.allGames = [Game]()
        self.filteredGames = [Game]()
        self.currentPage = 0
        self.gameRepository = GameRepository()
        self.favoriteRepository = FavoriteRepository()
        self.nameFilter = ""
        self.delegate = delegate
    }
    
    func viewDidLoad() {
        self.getFirstPage()
    }
    
    func getFirstPage() {
        if Connectivity.isConnectedToInternet {
            self.currentPage = 0
            self.allGames.removeAll()
            self.getGames()
        } else {
            self.delegate?.showNoInternetError()
        }
    }
    
    func getGames() {
        self.gameRepository.getGames(limit: self.pageSize, offset: self.currentPage * self.pageSize, success: { (games) in
            self.allGames += games
            self.currentPage += 1
            self.delegate?.endLoadData()
        }) {
            self.delegate?.show(message: String.localize(key: "message.error.load.more", comment: ""))
        }
    }
    
    func filterGames() {
        self.filteredGames = self.allGames.filter{ self.nameFilter.isEmpty || $0.name.contains(self.nameFilter) }
        self.delegate?.reloadData()
    }
}

//MARK: - UICollectionViewDelegate UICollectionViewDataSource
extension MainPresenter: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.filteredGames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameCell", for: indexPath) as! GameCell
        let game = self.filteredGames[indexPath.row]
        cell.delegate = self
        cell.fillCell(game: game, isFavorite: self.favoriteRepository.isFavorite(game: game))
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, willDisplay cell: UICollectionViewCell, forItemAt indexPath: IndexPath) {
        if !collectionView.hasActiveDrag && indexPath.row == self.filteredGames.count - 1 {
            self.getGames()
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        
        self.delegate?.showDetailsFor(game: self.filteredGames[indexPath.row])
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        let cellWidth = UIScreen.main.bounds.width / 3.75
        return CGSize(width: cellWidth, height: cellWidth)
    }
}

//MARK: - UICollectionViewDragDelegate
extension MainPresenter: UICollectionViewDragDelegate {
    func collectionView(_ collectionView: UICollectionView, itemsForBeginning session: UIDragSession, at indexPath: IndexPath) -> [UIDragItem] {
        let game = self.filteredGames[indexPath.row]
        if self.favoriteRepository.isFavorite(game: game) {
            return [UIDragItem]()
        } else {
            self.delegate?.show(message: String.localize(key: "message.drag.add.favorite", comment: ""))
            let itemProvider = NSItemProvider(object: "\(game.id)" as NSString)
            let dragItem = UIDragItem(itemProvider: itemProvider)
            dragItem.localObject = game
            return [dragItem]
        }
    }
    
    func collectionView(_ collectionView: UICollectionView, dragPreviewParametersForItemAt indexPath: IndexPath) -> UIDragPreviewParameters? {
        let previewParameters = UIDragPreviewParameters()
        previewParameters.visiblePath = UIBezierPath(rect: CGRect(x: 0, y: 0, width: 100, height: 100))
        return previewParameters
    }
}

//MARK: - UICollectionViewDropDelegate
extension MainPresenter: UICollectionViewDropDelegate {
    
    func collectionView(_ collectionView: UICollectionView, canHandle session: UIDropSession) -> Bool {
        return session.canLoadObjects(ofClass: NSString.self)
    }

    func collectionView(_ collectionView: UICollectionView, dropSessionDidUpdate session: UIDropSession, withDestinationIndexPath destinationIndexPath: IndexPath?) -> UICollectionViewDropProposal {
        return UICollectionViewDropProposal(operation: .move, intent: .unspecified)
    }
    
    func collectionView(_ collectionView: UICollectionView, performDropWith coordinator: UICollectionViewDropCoordinator) {
        if let object = coordinator.items.first?.dragItem.localObject,
            let game = object as? Game,
            let indexPath = coordinator.destinationIndexPath,
            let index = self.filteredGames.index(of: game)
        {
            if index  != indexPath.row {
                let cellIndexPath = IndexPath(row: index, section: 0)
                if let cell = collectionView.cellForItem(at: cellIndexPath),
                    let gameCell = cell as? GameCell
                {
                    self.changeFavoriteStatus(game: game, cell: gameCell)
                }
            }
        }
    }
}

//MARK: - GameCellProtocol
extension MainPresenter: GameCellProtocol {
    func changeFavoriteStatus(game: Game, cell: GameCell) {
        self.favoriteRepository.changeFavoriteStatus(game: game, success: {wasFavorited in
            cell.setFavorite(wasFavorited)
        }) {
            self.delegate?.show(message: String.localize(key: "message.error.default", comment: ""))
        }
    }
}

//MARK: - UISearchBarDelegate
extension MainPresenter: UISearchBarDelegate {
    func searchBar(_ searchBar: UISearchBar, textDidChange searchText: String) {
        self.nameFilter = searchText
        self.filterGames()
    }
}
