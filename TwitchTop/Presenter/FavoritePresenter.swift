import Foundation

protocol FavoritePresenterDelegate {
    func changeFavoriteStatus(wasFavorited: Bool)
    func show(message: String)
}

class FavoritePresenter {
    private var repository: FavoriteRepository
    private var delegate: FavoritePresenterDelegate?
    
    init(delegate: FavoritePresenterDelegate) {
        self.repository = FavoriteRepository()
        self.delegate = delegate
    }
    
    func isFavorite(game: Game) -> Bool {
        return self.repository.isFavorite(game: game)
    }
    
    func changeFavoriteStatus(game: Game) {
        self.repository.changeFavoriteStatus(game: game, success: {wasFavorited in 
            self.delegate?.changeFavoriteStatus(wasFavorited: wasFavorited)
        }) {
            self.delegate?.show(message: String.localize(key: "message.error.default", comment: ""))
        }
    }
}
