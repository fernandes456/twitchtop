import Foundation

struct TodayPresenter {
    
    private let repository: GameRepositoryType
    
    init(repository: GameRepositoryType = GameRepository()) {
        self.repository = repository
    }
    
    
    func getTopTreeGames(success: @escaping ([Game]) -> (),
                         failure: @escaping () -> ()) {
        
        self.repository.getGames(limit: 3, offset: 0, success: { (games) in
            success(games)
        }) {
            failure()
        }
    }
}
