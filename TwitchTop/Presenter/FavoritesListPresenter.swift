import Foundation
import UIKit

protocol FavoritesListPresenterDelegate {
    func reloadData()
    func showDetailsFor(game: Game)
    func show(message: String)
    func showEmptyList()
}

class FavoritesListPresenter: NSObject {
    private var favoriteGames: [Game]
    private var favoriteRepository: FavoriteRepository
    private var delegate: FavoritesListPresenterDelegate?
    
    init(delegate: FavoritesListPresenterDelegate) {
        self.delegate = delegate
        self.favoriteRepository = FavoriteRepository()
        self.favoriteGames = self.favoriteRepository.getAllFavoritesGames()
    }
    
    func reloadData() {
        self.favoriteGames = self.favoriteRepository.getAllFavoritesGames()
        if self.favoriteGames.count == 0 {
            self.delegate?.showEmptyList()
        } else {
            self.delegate?.reloadData()
        }
    }
}

extension FavoritesListPresenter: UICollectionViewDelegate, UICollectionViewDataSource {
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.favoriteGames.count
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "GameCell", for: indexPath) as! GameCell
        let game = self.favoriteGames[indexPath.row]
        cell.fillCell(game: game, isFavorite: true)
        cell.setDisableFavoriteButton()
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        collectionView.deselectItem(at: indexPath, animated: true)
        self.delegate?.showDetailsFor(game: self.favoriteGames[indexPath.row])
    }
}

extension FavoritesListPresenter: GameCellProtocol {
    func changeFavoriteStatus(game: Game, cell: GameCell) {
        self.favoriteRepository.changeFavoriteStatus(game: game, success: {wasFavorited in
            self.reloadData()
        }) {
            self.delegate?.show(message: String.localize(key: "message.error.default", comment: ""))
        }
    }
}
